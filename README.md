# nameless_terminal  
  
This is a terminal in gtk3 developed with python. It is not yet at a stable version.  
  
The documentation will be written if possible as soon as I make the version 1.0.  
I still need to make a window to modify the preferences to consider releasing the 1.0 version.  
    
As I use shortcuts as "Alt + Arrow, with nothing configured, I have some characters  
printed in the terminal when I open or switch between tabs. To circumvent this problem  
I have provided two files for zsh and bash:  
For zsh, add to your ~/.zshrc the binds provided in the zshrc file.  
If you use bash, copy the inputrc as ~/.inputrc  
These bindings will not be much of use if you use other shortcuts combinations.  
